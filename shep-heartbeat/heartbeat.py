import requests
import os

def main():
    # print("Pinging boards...")
    boards = requests.get("http://{}/api/{}/boards".format(
        os.getenv('SHEP_HOSTNAME'),
        os.getenv('API_VERSION')
    )).json()['results']
    for board in boards:
        try:
            r = requests.get("http://{}/devices".format(board['ip']))
            if (r.status_code == 200 and board['online'] == False):
                requests.patch("http://{}/api/{}/boards/{}".format(
                    os.getenv('SHEP_HOSTNAME'),
                    os.getenv('API_VERSION'),
                    board['uuid']
                ), data={'online': True})
        except requests.exceptions.ConnectionError:
            requests.patch("http://{}/api/{}/boards/{}".format(
                os.getenv('SHEP_HOSTNAME'),
                os.getenv('API_VERSION'),
                board['uuid']
            ), data={'online': False})
if __name__ == '__main__':
    main()
